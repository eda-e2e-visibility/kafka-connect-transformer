Kafka Connect SMT to add Kafka message key into value to be pushed to database table. 

This SMT supports inserting a record key into the record Value
Properties:

|Name|Description|Type|Default|Importance|
|---|---|---|---|---|
|`field.name`| Field name for key in the value struct | String | `key` | High |

Example on how to add to your connector:
```
transforms=insertKey
transforms.insertKey.type="com.hussein.samples.kafka.connect.smt.InsertKeyIntoValue$Value"
transforms.insertKey.field.name="key"
```


## Build

To build the JAR, you could use `mvn compile package`

## Reference in Kafka connect

You need to update `CONNECT_PLUGIN_PATH` in Kafka connect container to reference this transformer.
The following is an example

```
  kafka-connect:
    container_name: kafka-connect
    image: confluentinc/cp-kafka-connect:5.3.1
    hostname: kafka-connect
    ports:
      - 8083:8083
    depends_on:
      - zookeeper
      - kafka
      - schema-registry
    volumes:
    - $PWD/data/insert-key-1.0-SNAPSHOT.jar:/usr/share/java/custom-smt/insert-key-1.0-SNAPSHOT.jar
    environment:
      CONNECT_PLUGIN_PATH: '/usr/share/java,/usr/share/java/custom-smt'
```
Lots borrowed from the [How to Use Single Message Transforms in Kafka Connect](https://www.confluent.io/blog/kafka-connect-single-message-transformation-tutorial-with-examples/?_ga=2.200183790.1764857394.1579058506-1592101990.1551910865)